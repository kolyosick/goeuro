package com.kolyosick.goeuro;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

public class AutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    private ArrayList<String> _resultList;
    
    private final String API_BASE = "http://pre.dev.goeuro.de:12345/api/v1/suggest/position/en/name/";
    
    private final String KEY_GEO_POS = "geo_position";
    private final String KEY_LAT = "latitude";
    private final String KEY_LON = "longitude";
    
    public AutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }
    
    @Override
    public int getCount() {
        return _resultList.size();
    }

    @Override
    public String getItem(int index) {
        return _resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    _resultList = autocomplete(constraint.toString());
                    
                    // Assign the data to the FilterResults
                    filterResults.values = _resultList;
                    filterResults.count = _resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
    
    private ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            String requestUrl = API_BASE.concat(input);
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(Config.LOG_TAG, "Error processing API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(Config.LOG_TAG, "Error connecting to API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray resultsJsonArray = jsonObj.getJSONArray("results");

            //Sort results by distance if user position is available
            if (Config.userLocation != null) {
                JSONArray resultsJsonArraySorted;
                resultsJsonArraySorted = sortJsonArray(resultsJsonArray);
                resultsJsonArray = resultsJsonArraySorted;
            }

            resultList = new ArrayList<String>(resultsJsonArray.length());
            for (int i = 0; i < resultsJsonArray.length(); i++) {
                resultList.add(resultsJsonArray.getJSONObject(i).getString("name"));
            }
        } catch (JSONException e) {
            Log.e(Config.LOG_TAG, "Cannot process JSON results", e);
        }
        
        return resultList;
    }
    
    private JSONArray sortJsonArray(JSONArray array) throws JSONException {
        List<JSONObject> jsons = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            jsons.add(array.getJSONObject(i));
        }
        Collections.sort(jsons, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject a, JSONObject b) {
                float distanceToA = 0;
                float distanceToB = 0;

                try {
                    Location locationA = new Location("");
                    Location locationB = new Location("");
                    Location userLocation = Config.userLocation;

                    JSONObject geoA = a.getJSONObject(KEY_GEO_POS);
                    locationA.setLatitude(geoA.getDouble(KEY_LAT));
                    locationA.setLongitude(geoA.getDouble(KEY_LON));
                    distanceToA = userLocation.distanceTo(locationA);

                    JSONObject geoB = b.getJSONObject(KEY_GEO_POS);
                    locationB.setLatitude(geoB.getDouble(KEY_LAT));
                    locationB.setLongitude(geoB.getDouble(KEY_LON));
                    distanceToB = userLocation.distanceTo(locationB);
                } 
                catch (JSONException e) {
                    Log.e(Config.LOG_TAG, "JSONException in combineJSONArrays sort section", e);
                }

                if(distanceToA > distanceToB)
                    return 1;
                if(distanceToA < distanceToB)
                    return -1;
                return 0;
            }
        });
        return new JSONArray(jsons);
    }

}
