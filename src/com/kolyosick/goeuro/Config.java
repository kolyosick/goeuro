package com.kolyosick.goeuro;

import android.location.Location;

public class Config {

    public static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";
    public static final String LOG_TAG = "GoEuro";
    public static Location userLocation = null;

}
