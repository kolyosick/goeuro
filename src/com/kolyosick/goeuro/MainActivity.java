package com.kolyosick.goeuro;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

public class MainActivity extends Activity implements DatePickerFragment.TheListener, OnClickListener, OnItemClickListener {

    TextView _dateTextView;
    AutoCompleteTextView _from;
    AutoCompleteTextView _to;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        initUserLocation();
        
        Button dateButton= (Button) findViewById(R.id.date_button);
        Button searchButton= (Button) findViewById(R.id.search_button);
        _dateTextView = (TextView) findViewById(R.id.date_text_view);
        _from = (AutoCompleteTextView) findViewById(R.id.text_view_from);
        _to = (AutoCompleteTextView) findViewById(R.id.text_view_to);
        
        AutoCompleteAdapter adapter = new AutoCompleteAdapter(this, R.layout.list_item);
        _from.setAdapter(adapter);
        _to.setAdapter(adapter);
        
        SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_FORMAT_PATTERN);
        String currentDateandTime = sdf.format(new Date());
        _dateTextView.setText(currentDateandTime);
        
        dateButton.setOnClickListener(this);
        searchButton.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.date_button):
                DialogFragment picker = new DatePickerFragment();
                picker.show(getFragmentManager(), null);
                break;
            case (R.id.search_button):
                if (_from.length() == 0 || _to.length() == 0) {
                    return;
                }
                CharSequence toastMessage = getResources().getText(R.string.search_not_implemented);
                Toast toast = Toast.makeText(this, toastMessage, Toast.LENGTH_LONG);
                toast.show();
                break;
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        ((TextView) view).setText(str);
    }

    @Override
    public void setDate(String date) {
        _dateTextView.setText(date);
    }
    
    private void initUserLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null) {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        Config.userLocation = location;
    }

}
